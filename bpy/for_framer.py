import bpy

ob = bpy.data.objects["lines"]
me = ob.data
dron = bpy.data.objects["Dron"]

blokowanie=False

def clear():
    print(chr(27)+'[2j')
    print('\033c')
    print('\x1bc')


def lines():
    plik=open("lines[0].txt")
    inp=plik.read()
    tab=inp.split(" ", 30)  
    
    uno=0
    for vert in me.vertices: 
        new_location = vert.co
        new_location[0]=float(tab[(3*uno)+0])
        new_location[1]=float(tab[(3*uno)+1])
        new_location[2]=float(tab[(3*uno)+2])
        vert.co = new_location
        uno+=1


def drone():
    plik=open("dron[0].txt")
    inp=plik.read()
    tab=inp.split(" ", 30)
      
    dron.location.x = float(tab[0])
    dron.location.y = float(tab[1])
    dron.location.z = float(tab[2])
    
    

def my_frame(scene):  
    clear()
    lines()
    drone()
    
    
            
def register():
    bpy.app.handlers.frame_change_post.append(my_frame)

def unregister():
    bpy.app.handlers.frame_change_post.remove(my_frame)

register()
bpy.ops.screen.animation_play() 
