#pragma once
#include "common_types.hpp"
#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

namespace plane::D2 {
double distance(vector64F& p1, vector64F& p2);
double pythagorean_theorem(double x, double y);
}; // namespace plane::D2

namespace plane::D3 {
double distance(vector64F& p1, vector64F& p2);
};