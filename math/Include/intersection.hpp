#pragma once

#include "common_types.hpp"
#include <limits>
#include <math.h>

constexpr auto MAX_DOUBLE = std::numeric_limits<double>::max();

namespace plane::D2 {

class intersection {
public:
    static line create_line(vector64F& p1, vector64F& p2);
    static vector64F calculate(vector64F& p1, vector64F& p2, vector64F& p3, vector64F& p4);

private:
    static double calculate_cord(double a0, double a1, double b0, double b1);
};

} // namespace plane::D2

namespace plane::D3 {

class intersection {
public:
    static line create_line(vector64F& p1, vector64F& p2);
    static vector64F calculate(vector64F& p1, vector64F& p2, vector64F& p3, vector64F& p4);
    static vector64F approx_calculate(vector64F& p1, vector64F& p2, vector64F& p3, vector64F& p4);

private:
    static double calculate_cord(double x0, double x1, double a0, double a1);
};

} // namespace plane::D3
