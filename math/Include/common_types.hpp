#pragma once
#include <common_types.hpp>
#include <iostream>
#include <math.h>
constexpr double DELTA_ERROR = 0.01;

namespace plane::D2 {
template <typename T>
struct vector {
    T x;
    T y;

    vector& operator-(vector& rhs)
    {
        this->x -= rhs.x;
        this->y -= rhs.y;
        return *this;
    }

    vector& operator-(vector&& rhs)
    {
        this->x -= rhs.x;
        this->y -= rhs.y;
        return *this;
    }

    vector& operator+(vector& rhs)
    {
        this->x += rhs.x;
        this->y += rhs.y;
        return *this;
    }

    vector& operator+(vector&& rhs)
    {
        this->x += rhs.x;
        this->y += rhs.y;
        return *this;
    }

    bool operator==(const vector& rhs) const
    {
        return (fabs(this->x - rhs.x) <= DELTA_ERROR && fabs(this->y - rhs.y) <= DELTA_ERROR);
    }

    bool operator==(const vector&& rhs) const
    {
        return (fabs(this->x - rhs.x) <= DELTA_ERROR && fabs(this->y - rhs.y) <= DELTA_ERROR);
    }

    friend std::ostream& operator<<(std::ostream& o, const vector& v)
    {
        o << "x: " << v.x << ", y: " << v.y;
        return o;
    }
};

using vector64F = vector<double>;
struct line {
    vector64F base_point;
    vector64F directional_vector;
};

}; // namespace plane::D2

namespace plane::D3 {

template <typename T>
struct vector {
    T x;
    T y;
    T z;

    vector& operator-(vector& rhs)
    {
        this->x -= rhs.x;
        this->y -= rhs.y;
        this->z -= rhs.z;
        return *this;
    }

    vector& operator-(vector&& rhs)
    {
        this->x -= rhs.x;
        this->y -= rhs.y;
        this->z -= rhs.z;
        return *this;
    }

    vector& operator+(vector& rhs)
    {
        this->x += rhs.x;
        this->y += rhs.y;
        this->z += rhs.z;
        return *this;
    }

    vector& operator+(vector&& rhs)
    {
        this->x += rhs.x;
        this->y += rhs.y;
        this->z += rhs.z;
        return *this;
    }

    bool operator==(const vector& rhs) const
    {
        return (fabs(this->x - rhs.x) <= DELTA_ERROR && fabs(this->y - rhs.y) <= DELTA_ERROR && fabs(this->z - rhs.z) <= DELTA_ERROR);
    }

    bool operator==(const vector&& rhs) const
    {
        return (fabs(this->x - rhs.x) <= DELTA_ERROR && fabs(this->y - rhs.y) <= DELTA_ERROR && fabs(this->z - rhs.z) <= DELTA_ERROR);
    }

    friend std::ostream& operator<<(std::ostream& o, const vector& v)
    {
        o << "x: " << v.x << ", y: " << v.y << ", z: " << v.z;
        return o;
    }
};

using vector64F = vector<double>;

struct line {
    vector64F base_point;
    vector64F directional_vector;
};

}; // namespace plane::D3
