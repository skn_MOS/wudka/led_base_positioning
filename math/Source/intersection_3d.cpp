#include "intersection.hpp"
#include "tools.hpp"
#include <stdio.h>

namespace plane::D3 {
line intersection::create_line(vector64F& p1, vector64F& p2)
{
    line l;
    l.base_point = p1;
    l.directional_vector = p2 - p1;

    return l;
}

double intersection::calculate_cord(double x0, double x1, double a0, double a1)
{
    // I expect here floating point numbers inaccurancy
    // when points has no intersection
    // just return error
    if (fabs(a1 - a0) < 0.000001) return MAX_DOUBLE;

    return (x0 * a1 - x1 * a0) / (a1 - a0);
}

vector64F intersection::calculate(vector64F& p1, vector64F& p2, vector64F& p3, vector64F& p4)
{
    auto l0 = create_line(p1, p2);
    auto l1 = create_line(p3, p4);

    vector64F intersection_point;
    intersection_point.x =
        calculate_cord(l0.base_point.x, l1.base_point.x, l0.directional_vector.x, l1.directional_vector.x);

    intersection_point.y =
        calculate_cord(l0.base_point.y, l1.base_point.y, l0.directional_vector.y, l1.directional_vector.y);

    intersection_point.z =
        calculate_cord(l0.base_point.z, l1.base_point.z, l0.directional_vector.z, l1.directional_vector.z);

    return intersection_point;
}

vector64F intersection::approx_calculate(vector64F& p1, vector64F& p2, vector64F& p3, vector64F& p4)
{
    // project points on 2d plane
    D2::vector64F pp1 = {p1.x, p1.y};
    D2::vector64F pp2 = {p2.x, p2.y};
    D2::vector64F pp3 = {p3.x, p3.y};
    D2::vector64F pp4 = {p4.x, p4.y};

    // calculate intersection ( X, Y )
    auto intersect_point_2d = plane::D2::intersection::calculate(pp1, pp2, pp3, pp4);

    // calculate all distances necessary for Intercept theorem (TALES)
    auto x_dist = plane::D2::pythagorean_theorem(p1.x - intersect_point_2d.x, p1.y - intersect_point_2d.y);
    auto y_dist = plane::D2::pythagorean_theorem(p1.x - p2.x, p1.y - p2.y);
    auto height = p2.z - p1.z;

    if (!unlikely(y_dist)) return {MAX_DOUBLE, MAX_DOUBLE, MAX_DOUBLE};
    // calculate z from proportion
    auto z_offset = (height * x_dist) / y_dist;

    // return aproximated point in 3d space
    return {intersect_point_2d.x, intersect_point_2d.y, z_offset + (p1.z + p3.z) / 2};
}

} // namespace plane::D3
