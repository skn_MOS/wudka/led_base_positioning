#include "tools.hpp"
#include <math.h>

namespace plane::D2 {
double distance(vector64F& p1, vector64F& p2)
{
    return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

double pythagorean_theorem(double x, double y)
{
    return sqrt(x * x + y * y);
}

}; // namespace plane::D2

namespace plane::D3 {
double distance(vector64F& p1, vector64F& p2)
{
    return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y) + (p2.z - p1.z) * (p2.z - p1.z));
}
} // namespace plane::D3