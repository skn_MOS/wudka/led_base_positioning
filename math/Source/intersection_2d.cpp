#include <intersection.hpp>

namespace plane::D2 {
line intersection::create_line(vector64F& p1, vector64F& p2)
{
    line l;
    l.base_point = p1;
    l.directional_vector = p2 - p1;

    return l;
}

double intersection::calculate_cord(double a0, double a1, double b0, double b1)
{
    // I expect here floating point numbers inaccurancy
    // when points has no intersection
    // just return error
    if (fabs(a1 - a0) < 0.0000001) return MAX_DOUBLE;

    return (b0 - b1) / (a1 - a0);
}

vector64F intersection::calculate(vector64F& p1, vector64F& p2, vector64F& p3, vector64F& p4)
{
    auto a1 = p2.y - p1.y;
    auto b1 = p1.x - p2.x;
    auto c1 = a1 * p1.x + b1 * p1.y;

    auto a2 = p4.y - p3.y;
    auto b2 = p3.x - p4.x;
    auto c2 = a2 * (p3.x) + b2 * (p3.y);

    auto determinant = a1 * b2 - a2 * b1;

    if (fabs(determinant) <= 0.0000001) {
        return {MAX_DOUBLE, MAX_DOUBLE};
    }

    auto x = (b2 * c1 - b1 * c2) / determinant;
    auto y = (a1 * c2 - a2 * c1) / determinant;
    return {x, y};
}

}; // namespace plane::D2