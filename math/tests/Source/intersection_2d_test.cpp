#include <gtest/gtest.h>
#include <intersection.hpp>

using vector_2d = plane::D2::vector64F;
using intersectio_2d = plane::D2::intersection;

TEST(IntersectionTest, Handle2DIntegerIntersection)
{
    vector_2d A = {-4.0, -2.0};
    vector_2d B = {5.0, 7.0};
    vector_2d C = {8.0, -2.0};
    vector_2d D = {0.0, 6.0};

    vector_2d controll_point = {2, 4};

    auto res = intersectio_2d::calculate(A, B, C, D);
    EXPECT_EQ(res, controll_point);

    A = {-16.0, -2.0};
    B = {0.0, 12.0};
    C = {2.0, -10.0};
    D = {0.0, 12.0};

    controll_point = {0, 12};

    res = intersectio_2d::calculate(A, B, C, D);
    EXPECT_EQ(res, controll_point);

    A = {0.0, 0.0};
    B = {-30'000'000.0, 20'000'000.0};
    C = {42'000'000, 14'000'000};
    D = {-11'000'000, 14'000'000};

    controll_point = {-21'000'000, 14'000'000};

    res = intersectio_2d::calculate(A, B, C, D);
    EXPECT_EQ(res, controll_point);
}

TEST(IntersectionTest, Handle2DEdgeIntesection)
{
    vector_2d A = {2.7334658927692, -16.8192988831062};
    vector_2d B = {25.1271470635412, 4.8243546886454};
    vector_2d C = {24.1628258647998, -15.319243685064};
    vector_2d D = {3.2691998920699, 4.9315014885055};

    vector_2d controll_point = {14.2381482754413, -5.6999407907625};

    auto res = intersectio_2d::calculate(A, B, C, D);
    EXPECT_EQ(res, controll_point);

    A = {0.0, 2.5};
    B = {0.0, 0.0};
    C = {2.5, 0.0};
    D = {-2.7, 1.57};

    controll_point = {0.0, 0.75};
    res = intersectio_2d::calculate(A, B, C, D);

    EXPECT_EQ(res, controll_point);

    A = {-1509304.8059129309841, -345252.0509642603574};
    B = {-327741.9036687701591, 2270454.2290038387291};
    C = {507057.9729167782934, -918909.4020538270706};
    D = {-2155739.5821407148615, -721982.2516797967255};
    controll_point = {-1694876.7723033088259, -756065.353371957768};

    res = intersectio_2d::calculate(A, B, C, D);
    EXPECT_EQ(res, controll_point);
}

TEST(IntersectionTest, Handle2DNoIntersection)
{
    // NOTICE !!!!
    // ONLY X DETERMINE IF TWO LINES INTERSECT
    // IF X == MAX_DOUBLE, LINES DO NOT INTERSECTS

    vector_2d A = {-100'000, 0};
    vector_2d B = {0, 50'000};
    vector_2d C = {0, -150'000};
    vector_2d D = {300'000, 0};

    vector_2d controll_point = {MAX_DOUBLE, 0};
    auto res = intersectio_2d::calculate(A, B, C, D);

    EXPECT_EQ(res.x, controll_point.x) << res.x << controll_point.x;

    A = {-3.5853789252984, -9.9502590995595};
    B = {7.4457580728701, 1.542646424114};
    C = {-6.9716814456664, -6.1021880536867};
    D = {3.2276175591132, 4.5240583512933};

    controll_point = {MAX_DOUBLE, 0};
    res = intersectio_2d::calculate(A, B, C, D);

    EXPECT_EQ(res.x, controll_point.x) << res.x << controll_point.x;
}
