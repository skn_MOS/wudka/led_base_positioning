#include <gtest/gtest.h>
#include <intersection.hpp>

using vector_3d = plane::D3::vector64F;
using intersectio_3d = plane::D3::intersection;

TEST(IntersectionTest, Handle3DIntegerIntersection)
{
    // has exac intersection
    vector_3d A = {4.0, 0.0, 0.0};
    vector_3d B = {0.0, 0.0, 3.0};
    vector_3d C = {0.0, -3.0, 0.0};
    vector_3d D = {2.0, 3.0, 4.5};

    vector_3d controll_point = {1.0, 0.0, 2.25};

    auto res = intersectio_3d::approx_calculate(A, B, C, D);
    EXPECT_EQ(res, controll_point);

    // doesn't have exac intersection
    A = {0.0, 2.5, 0.0};
    B = {0.0, 0.0, 3.0};
    C = {2.5, 0.0, 0.0};
    D = {-2.7, 1.57, 3.25};

    controll_point = {0.0, 0.75, 2.1};

    res = intersectio_3d::approx_calculate(A, B, C, D);
    EXPECT_EQ(res, controll_point);
}