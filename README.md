# Led based positioning

## Requirements
 - g++
 ```apt install g++```
 - installed cmake 3.10+
 ```apt install cmake```
 - installed opencv 4.1+
 `apt install opencv` &&
 `apt install opencv-dev`
 - installed fmt-dev 5.0+
 ```apt install fmt-dev```  
 - Google Test framework
 `apt install gtest` && `apt install gtest-dev`

 - Apart form software, at least two cameras are required.


## Compilation
 1. mkdir build && cd build
 2. ```cmake -DCMAKE_BUILD_TYPE=Debug .. ``` or ```cmake -DCMAKE_BUILD_TYPE=Release ..```     
 2.1 When building with tests -> ```cmake -DCMAKE_BUILD_TYPE=(Debug|Release) -DMATH_TESTS=ON  ..```  
 2.2 When additional debug messages are necessary, just add ```-DDEBUG_MSG=ON```  
 2.3 For performance debuging, just add ```-DTEST_PERFORMANCE=ON```  
 2.4 For increasing performance but decreasing debug, just add ```-DDISABLE_VIUALISATION=ON```  
 3. `make -j$(nproc)`
