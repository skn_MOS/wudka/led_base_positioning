#include "controller.hpp"

#include "parser/argparse.hpp"
#include <fmt/format.h>
#include <stdio.h>

using namespace std;

int main(int argc, const char* argv[])
{
    ArgumentParser parser;
    // add some arguments to search for
    parser.addArgument("-c", "--config", 1);
    // parse the command-line arguments - throws if invalid format
    parser.parse(argc, argv);

    std::string configPath = "";
    if (argc != 1)
        // if we get here, the configuration is valid
        configPath = parser.retrieve<std::string>("config");

    core::controller inst(configPath);
    inst.run();

    return 0;
}
