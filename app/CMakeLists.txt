cmake_minimum_required(VERSION 3.10)
project(base_led_position)

include_directories(../math/Include)
find_package( OpenCV REQUIRED )

file( GLOB SRC "Source/*.cpp")

add_library(vision_core ${SRC})

if (${DEBUG_MSG})
	MESSAGE("-- Debug meassages activated")
	target_compile_definitions(vision_core PUBLIC -DPRINT_DEBUG_MSG)
endif()

if (${DISABLE_VIUALISATION})
	MESSAGE("-- Visualisation disabled")
	target_compile_definitions(vision_core PUBLIC -DVISUALISATION=0)
else()
	target_compile_definitions(vision_core PUBLIC -DVISUALISATION=1)
endif()


target_include_directories(vision_core PUBLIC ${OpenCV_INCLUDE_DIRS})
target_include_directories(vision_core PUBLIC Include)

target_link_libraries(vision_core ${OpenCV_LIBS} stdc++fs)
