#include "camera.hpp"
#include "image_utils.hpp"
#include "sync.hpp"
#include <fmt/format.h>
#include <space_utils.hpp>

using namespace cv;
namespace core::camera {
using namespace core::camera::config;
using namespace core::ui::config;
using namespace core::image;

camera_t::camera_t(int camId, const bool& finishedSignal) : isFinished(&finishedSignal)
{
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][camera_t::camera_t()][STARTING]\n");
#endif
    cam.open(camId);
    if (!cam.isOpened()) {
        fmt::print("Can't open camera {}\n", camId);
        return;
    }

    currentCammera = next_camera_id++;
    cam.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    cam.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
    currentFrame = buffer;
    pointPosOnPlane = convertToPointInSpace(trackedPoint.x, trackedPoint.y);

    startService();
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][camera_t::operator=][CAMID] {}\n", currentCammera);
    fmt::print("[FUNCTION][camera_t::camera_t()][LEAVING]\n");
#endif
}

camera_t::camera_t(int camId, pointF_3D camPos, ref2DPointArray virtualRefPos, ref3DPointArray realRefPos, const bool& finishedSignal)
    : camera_t(camId, finishedSignal)
{
    realCameraPos = camPos;
    for (auto i = 0; i < config::MAX_REFERENCE_POINTS; i++) {
        referencePoints[i] = virtualRefPos[i];
        realPointReference[i] = realRefPos[i];
    }
}

camera_t& camera_t::operator=(camera_t&& c)
{
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][camera_t::operator=][STARTING]\n");
#endif
    cam = std::move(c.cam);
    currentCammera = c.currentCammera;

    if (!cam.isOpened()) return *this;

    bool finish = true;
    const bool* isFinishedCopy = c.isFinished;
    c.isFinished = &finish;
    c.getNextFrame.store(true, std::memory_order_relaxed);
    // wait for threads
    if (c.cameraFeeder.joinable()) c.cameraFeeder.join();
    if (c.imageProcessor.joinable()) c.imageProcessor.join();

    realCameraPos = c.realCameraPos;

    for (auto i = 0; i < config::MAX_REFERENCE_POINTS; i++) {
        referencePoints[i] = c.referencePoints[i];
        realPointReference[i] = c.realPointReference[i];
    }

    isFinished = isFinishedCopy;
    currentFrame = buffer;

    c.currentFrame = nullptr;
    c.frameToRender = nullptr;
    startService();
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][camera_t::operator=][CAMID] {}\n", currentCammera);
    fmt::print("[FUNCTION][camera_t::operator=][LEAVING]\n");
#endif

    return *this;
}

void camera_t::startService()
{
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][startService][STARTING]\n");
#endif

    frameToRender = currentFrame;
    frameHSVToRender = currentFrame;
    thresholdFrameToRender = currentFrame;

    cam.read(*currentFrame);
    cameraFeeder = std::thread(&camera_t::getImage, this);
    imageProcessor = std::thread(&camera_t::processImage, this);

#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][startService][LEAVING]\n");
#endif
}

camera_t::~camera_t()
{
    // just to be sure that we didn't stuck in camera loops
    bool finish = true;
    isFinished = &finish;
    getNextFrame.store(true, std::memory_order_relaxed);
    if (cameraFeeder.joinable()) cameraFeeder.join();
    if (imageProcessor.joinable()) imageProcessor.join();
}

void camera_t::getImage()
{
    Mat* current_frame_copy;
    int nextFrameId = 0;
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][getImage][STARTING][id: {}]\n", currentCammera);
#endif
    while (!*isFinished) {
        while (!getNextFrame) std::this_thread::sleep_for(CAPTURE_IMAGE_DELAY);
        nextFrameId ^= 0x1;
        current_frame_copy = &buffer[nextFrameId];
        getNextFrame.store(false, std::memory_order_relaxed);
        cam.read(*current_frame_copy);
        currentFrame = current_frame_copy;
    }
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][getImage][FINISED][id: {}]\n", currentCammera);
#endif
}

void camera_t::drawLines(Mat& frame)
{
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][drawLines][STARTING]\n");
#endif
    line(frame, Point(referencePoints[0].x, referencePoints[0].y), Point(referencePoints[1].x, referencePoints[1].y),
         Scalar(0, 255, 0), 2);
    line(frame, Point(referencePoints[2].x, referencePoints[2].y), Point(referencePoints[1].x, referencePoints[1].y),
         Scalar(0, 255, 0), 2);
}

void camera_t::processImage()
{
    auto& inter_obj = intersection_manager::get_instance();
    auto& cam_sync = camera_synchronizer::get_instance();
    int nextFrameId = 0;
    Mat threshold[config::MAX_CAMERA_FRAMES_COUNT], HSV[config::MAX_CAMERA_FRAMES_COUNT];

    Mat* frame = loadNextFrame();
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][processImage][STARTING][id: {}]\n", currentCammera);
#endif
    while (!*isFinished) {
        cvtColor(*frame, HSV[nextFrameId], COLOR_BGR2HSV);
        inRange(HSV[nextFrameId], Scalar(cr.H_MIN, cr.S_MIN, cr.V_MIN), Scalar(cr.H_MAX, cr.S_MAX, cr.V_MAX), threshold[nextFrameId]);
        morphOps(threshold[nextFrameId]);
        if (getTrackedObjectPosition(trackedPoint.x, trackedPoint.y, threshold[nextFrameId], *frame)) {
            pointPosOnPlane = convertToPointInSpace(trackedPoint.x, trackedPoint.y);
        }

#if VISUALISATION == 1
        drawLines(*frame);
#endif

        frameToRender = frame;
        frameHSVToRender = &HSV[nextFrameId];
        thresholdFrameToRender = &threshold[nextFrameId];

        nextFrameId ^= 0x1;
        frame = loadNextFrame();

        if (cam_sync.getState(currentCammera) != line_state::READY) {
            if (!cam_sync.mut.try_lock()) continue;
            cam_sync.newLineAdded(currentCammera);
            inter_obj.add_line(realCameraPos, pointPosOnPlane);
            cam_sync.mut.unlock();
        }

        std::this_thread::sleep_for(CAPTURE_IMAGE_DELAY);
    }
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][processImage][FINISED][id: {}]\n", currentCammera);
#endif
}

Mat* camera_t::loadNextFrame()
{
    Mat* frame = currentFrame;
    getNextFrame.store(true, std::memory_order_relaxed);
    return frame;
}

void camera_t::frame_debug()
{
    imwrite("../blender/frame[" + std::to_string(currentCammera) + "].jpg", *currentFrame);
}

void camera_t::plane_debug()
{
    std::ofstream file("../blender/plane[" + std::to_string(currentCammera) + "].txt", std::fstream::out);

    for (int yrange = 0; yrange <= FRAME_WIDTH; yrange += FRAME_WIDTH) {
        for (int xrange = 0; xrange <= FRAME_HEIGHT; xrange += FRAME_HEIGHT) {
            auto point = convertToPointInSpace(xrange, yrange);
#ifdef PRINT_DEBUG_MSG
            fmt::print("5ta5_plane_debug_input({} , {})", xrange, yrange);
            fmt::print("5ta5_plane_debug_generated_output[{}, {} {}]", point.x, point.y, point.z);
#endif
            std::string s = fmt::format("{} {} {} ", point.x, point.y, point.z);
            file << s;
        }
    }
    file.close();
}

pointF_3D camera_t::convertToPointInSpace(int inp_x, int inp_y)
{
    auto ans = realPointReference[1];

    float tmp_reference_x = (float(inp_x - referencePoints[1].y) / float(referencePoints[2].y - referencePoints[1].y)); // wysokosc
    float tmp_reference_y = (float(inp_y - referencePoints[1].x) / float(referencePoints[0].x - referencePoints[1].x)); // szerokosc

    ans.x += (tmp_reference_y * (realPointReference[0].x - realPointReference[1].x));
    ans.y += (tmp_reference_y * (realPointReference[0].y - realPointReference[1].y));
    ans.z += (tmp_reference_x * (realPointReference[2].z - realPointReference[1].z));
    return (ans);
}
} // namespace core::camera
