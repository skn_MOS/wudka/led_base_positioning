#include "config.hpp"
namespace core::ui::config {
const std::string mainWindow = "OI_W";
const std::string hsvWindow = "HSV_W";
const std::string thresholdedWindow = "TI_W";
const std::string MorphologicalWindow = "AMO_W";
const std::string trackbarWindow = "Trackbars";
} // namespace core::ui::config