#include "user_manager.hpp"
#include "image_utils.hpp"
#include "space_utils.hpp"
#include "sync.hpp"
#include <chrono>

#include <opencv2/opencv.hpp>

namespace core::ui {
using namespace core::camera::config;
using namespace core::ui::config;
using namespace cv;

void userManager::registerCamera(core::camera::camera_t* c)
{
    if (cameraCount >= MAX_CAMERA_COUNT) return;

    camera_ref[cameraCount] = c;
    cameraCount++;
}

void userManager::createTrackers()
{
    core::image::createTrackbars(camera_ref[0]->cr);
}

void userManager::startService()
{
    auto& inter_obj = core::camera::intersection_manager::get_instance();
    auto& cam_sync = camera_synchronizer::get_instance();
    auto last_time = std::chrono::system_clock::now();
    auto current_time = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds;
    int key;
#if VISUALISATION == 1
    Mat *frameCopy, *HSVCopy, *thresholdCopy;
#endif
    while (!isFinished) {
#if VISUALISATION == 1
        frameCopy = camera_ref[activeCameraId]->frameToRender;
        HSVCopy = camera_ref[activeCameraId]->frameHSVToRender;
        thresholdCopy = camera_ref[activeCameraId]->thresholdFrameToRender;
        imshow(mainWindow, *frameCopy);
        imshow(hsvWindow, *HSVCopy);
        imshow(thresholdedWindow, *thresholdCopy);
#endif
        current_time = std::chrono::system_clock::now();
        elapsed_seconds = current_time - last_time;
        if (elapsed_seconds.count() > 1.0) {
            cam_sync.mut.lock();
            inter_obj.clear_lines();
            cam_sync.reset_all();
            cam_sync.mut.unlock();
            last_time = current_time;
        }
        else if (inter_obj.lines_count() >= 2) {
            inter_obj.calculate_position_3d();
            inter_obj.debug_lines();
            inter_obj.debug_point();
            cam_sync.mut.lock();
            inter_obj.clear_lines();
            cam_sync.reset_all();
            cam_sync.mut.unlock();
            last_time = current_time;
        }
        key = waitKey(BASE_DELAY);
        if (key != -1) handleKeyboard(key);
    }
}

void userManager::handleKeyboard(int key)
{
    switch (key) {
    case '1':
        if (setActiveCamera(0)) {
            destroyWindow(core::ui::config::trackbarWindow);
            core::image::createTrackbars(camera_ref[activeCameraId]->cr); // a bit shitty way
        }
        break;
    case '2':
        if (setActiveCamera(1)) {
            destroyWindow(core::ui::config::trackbarWindow);
            core::image::createTrackbars(camera_ref[activeCameraId]->cr); // a bit shitty way
        }
        break;
    case 'f': // first
        camera_ref[activeCameraId]->captureReferencePoint(0);
        camera_ref[activeCameraId]->frame_debug();
        camera_ref[activeCameraId]->plane_debug();
        break;
    case 's': // second
        camera_ref[activeCameraId]->captureReferencePoint(1);
        camera_ref[activeCameraId]->frame_debug();
        camera_ref[activeCameraId]->plane_debug();
        break;
    case 't': // third
        camera_ref[activeCameraId]->captureReferencePoint(2);
        camera_ref[activeCameraId]->frame_debug();
        camera_ref[activeCameraId]->plane_debug();
        break;
    case 'q':
    case 27: // ESC
    {
        isFinished = true;
        break;
    }
    }
}

} // namespace core::ui
