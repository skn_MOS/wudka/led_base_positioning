#include "space_utils.hpp"
#include <fmt/format.h>
#include <fstream>

using intersection_3d = plane::D3::intersection;
namespace core::camera {
void intersection_manager::clear_lines()
{
    lines.clear();
}

void intersection_manager::add_line(pointF_3D& a, pointF_3D& b) noexcept
{
#ifdef PRINT_DEBUG_MSG
    fmt::print(
        "[FUNCTION][intersection_manager::add_line()][APPENDING]: ({}, {}, {})---------------------({}, {}, {})\n", a.x,
        a.y, a.z, b.x, b.y, b.z);
#endif
    if constexpr (config::MAX_CAMERA_COUNT > 2) {
        std::sort(lines.begin(), lines.current_pos(), std::less<line_t>());
    }
    lines.push_back({a, b});
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][intersection_manager::add_line()][CURRENT SIZE]: {})\n", lines.current_size());
#endif
}

void intersection_manager::calculate_position_3d()
{
    position_3d = intersection_3d::approx_calculate(lines[0].first, lines[0].second, lines[1].first, lines[1].second);
}

void intersection_manager::debug_lines()
{
    std::ofstream file("../blender/lines[0].txt", std::fstream::out);
    std::string s;
    for (int i = 0; i < 2; i++) {
        s = fmt::format("{} {} {} {} {} {} ", lines[i].first.x, lines[i].first.y, lines[i].first.z, lines[i].second.x,
                        lines[i].second.y, lines[i].second.z);
        file << s;
    }
}

void intersection_manager::debug_point()
{
    std::ofstream file("../blender/dron[0].txt", std::fstream::out);
    std::string s = fmt::format("{} {} {} ", position_3d.x, position_3d.y, position_3d.z);
    file << s;
}
} // namespace core::camera
