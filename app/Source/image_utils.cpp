#include "image_utils.hpp"
#include "config.hpp"

#include <cmath>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;
using namespace core::ui::config;

using namespace core::camera;

namespace core::image {
constexpr int MAX_HSV_VAL = 255;

// This function gets called whenever a
// trackbar position is changed
void on_trackbar(int, void*)
{
}

void createTrackbars(color_range& cr)
{
    // create window for trackbars
    namedWindow(trackbarWindow, 0);
    createTrackbar("H_MIN", trackbarWindow, &cr.H_MIN, MAX_HSV_VAL, on_trackbar);
    createTrackbar("H_MAX", trackbarWindow, &cr.H_MAX, MAX_HSV_VAL, on_trackbar);
    createTrackbar("S_MIN", trackbarWindow, &cr.S_MIN, MAX_HSV_VAL, on_trackbar);
    createTrackbar("S_MAX", trackbarWindow, &cr.S_MAX, MAX_HSV_VAL, on_trackbar);
    createTrackbar("V_MIN", trackbarWindow, &cr.V_MIN, MAX_HSV_VAL, on_trackbar);
    createTrackbar("V_MAX", trackbarWindow, &cr.V_MAX, MAX_HSV_VAL, on_trackbar);
}

static void drawObject(int x, int y, Mat& frame)
{
    circle(frame, Point(x, y), 20, Scalar(0, 255, 0), 2);
    if (y - 25 > 0)
        line(frame, Point(x, y), Point(x, y - 25), Scalar(0, 255, 0), 2);
    else
        line(frame, Point(x, y), Point(x, 0), Scalar(0, 255, 0), 2);
    if (y + 25 < FRAME_HEIGHT)
        line(frame, Point(x, y), Point(x, y + 25), Scalar(0, 255, 0), 2);
    else
        line(frame, Point(x, y), Point(x, FRAME_HEIGHT), Scalar(0, 255, 0), 2);
    if (x - 25 > 0)
        line(frame, Point(x, y), Point(x - 25, y), Scalar(0, 255, 0), 2);
    else
        line(frame, Point(x, y), Point(0, y), Scalar(0, 255, 0), 2);
    if (x + 25 < FRAME_WIDTH)
        line(frame, Point(x, y), Point(x + 25, y), Scalar(0, 255, 0), 2);
    else
        line(frame, Point(x, y), Point(FRAME_WIDTH, y), Scalar(0, 255, 0), 2);

    putText(frame, std::to_string(x) + "," + std::to_string(y), Point(x, y + 30), 1, 1, Scalar(0, 255, 0), 2);
}

void morphOps(Mat& thresh)
{
    // create structuring element that will be used to "dilate" and "erode" image.
    // the element chosen here is a 3px by 3px rectangle
    Mat erodeElement = getStructuringElement(MORPH_RECT, Size(3, 3));
    // dilate with larger element so make sure object is nicely visible
    Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8, 8));

    erode(thresh, thresh, erodeElement);
    erode(thresh, thresh, erodeElement);

    dilate(thresh, thresh, dilateElement);
    dilate(thresh, thresh, dilateElement);
}

bool getTrackedObjectPosition(int& x, int& y, Mat& threshold, Mat& cameraFeed)
{
    Mat temp;
    threshold.copyTo(temp);
    // these two vectors needed for output of findContours
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    // find contours of filtered image using openCV findContours function
    findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    // use moments method to find our filtered object

    double refArea = 0;
    bool objectFound = false;
    if (hierarchy.size() > 0) {
        int numObjects = hierarchy.size();
        // if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
        if (numObjects < MAX_NUM_OBJECTS) {
            for (int index = 0; index >= 0; index = hierarchy[index][0]) {
                Moments moment = moments((cv::Mat)contours[index]);
                double area = moment.m00;

                // if the area is less than 20 px by 20px then it is probably just noise
                // if the area is the same as the 3/2 of the image size, probably just a bad filter
                // we only want the object with the largest area so we safe a reference area each
                // iteration and compare it to the area in the next iteration.
                objectFound = false;

                if (area > MIN_OBJECT_AREA && area < MAX_OBJECT_AREA && area > refArea) {
                    x = moment.m10 / area;
                    y = moment.m01 / area;
                    objectFound = true;
                    refArea = area;
                }
            }
            // let user know you found an object
            if (objectFound == true) {
                putText(cameraFeed, "Tracking Object", Point(0, 50), 2, 1, Scalar(0, 255, 0), 2);
                // draw object location on screen
                drawObject(x, y, cameraFeed);
                return true;
            }
        }
        else
            putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
    }
    return false;
}
} // namespace core::image
