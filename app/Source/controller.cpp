#include "controller.hpp"
#include "camera.hpp"
#include "rapidjson/document.h"
#include <cstdlib>
#include <fmt/format.h>

namespace fs = std::filesystem;
using namespace rapidjson;

namespace core {
controller::controller(std::string& c) : uiManager(), finished(uiManager.getStatusRef())
{
    int i = 0;
    if (c == "") {
        for (; i < camera::config::MAX_CAMERA_COUNT; i++) {
            cameras[i] = camera::camera_t(CAM_0 + i * 2, finished);
            if (cameras[i].isActive()) {
                uiManager.registerCamera(&cameras[i]);
                cameras[i].plane_debug();
                cameras[i].frame_debug();
            }
            else // when n'th camera is not accesible, we should break connecting cameras
                break;
        }
    }
    else {
        auto files = load_directory_files(c.c_str());
        for (auto f : files) {
            if (i == camera::config::MAX_CAMERA_COUNT) break;
            cameras[i] = loadCam(f.c_str());
            if (cameras[i].isActive()) {
                uiManager.registerCamera(&cameras[i]);
                cameras[i].plane_debug();
                cameras[i].frame_debug();
            }
            else // when n'th camera is not accesible, we should break connecting cameras
                break;
            i++;
        }
    }
    if (i < 2) {
        fmt::print(stderr, "Not enought active cameras found\n");
        abort();
    }
    uiManager.createTrackers();
}

void controller::run()
{
    uiManager.startService();
}

directory_info_t controller::load_directory_files(const char* dir_path)
{
    directory_info_t info;
    for (auto& p : fs::directory_iterator(dir_path)) info.emplace_back(p.path());
    return info;
}

/*
class config_manager(){
  load();
  get(string what){

  }
  set(){

  }
  save();
};

camera::camera_t controller::loadCam(const char* fileName){
  refeREFERENCE_PO=get("aasdadasd")
  refeREFERENCE_PO=get("aasdadasd")
  refeREFERENCE_PO=get("aasdadasd")
  refeREFERENCE_PO=get("aasdadasd")
}*/

camera::camera_t controller::loadCam(const char* fileName)
{
#ifdef PRINT_DEBUG_MSG
    fmt::print(stderr, "[FUNCTION][loadCam]\n");
#endif

    int camID;
    camera::pointF_3D camPos;
    camera::ref3DPointArray realRefPoints;
    camera::ref2DPointArray virtualRefPoints;
    std::string fileText;
    Document document;

    auto fileSize = getFileSize(fileName);
    if (fileSize == -1) {
        fmt::print(stderr, "Can't open file 1!\n");
        return {};
    }

    fileText.resize(fileSize + 1); // +1 for terminator
    std::fstream fs(fileName, std::fstream::in | std::fstream::out);

    if (!fs.is_open()) {
        fmt::print(stderr, "Can't open file 2!\n");
        return {};
    }
    fs.read(fileText.data(), fileSize);

#ifdef PRINT_DEBUG_MSG
    fmt::print(stderr, "[FUNCTION][loadCam][FILE_CONTENT]\n{}\n", fileText);
#endif
    // parse document for desired values
    document.Parse(fileText.c_str());
    assert(document.IsObject());

    std::string v = document["version"].GetString();
    if (v != CONFIG_CURRENT_VERSION) {
        fmt::print(stderr, "FILE VERSION DOES NOT MATCH {} != {}\n", v, CONFIG_CURRENT_VERSION);
        return {};
    }

    camID = document["camera_id"].GetInt();
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][loadCam][VAR][camID][{}]\n", camID);
#endif

    const auto& pos = document["camera_position"];
    camPos.x = pos[0].GetDouble();
    camPos.y = pos[1].GetDouble();
    camPos.z = pos[2].GetDouble();
#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][loadCam][VAR][camPos][{} {} {}]\n", camPos.x, camPos.y, camPos.z);
#endif

    const auto& real_pos = document["real_point_references"];

    for (int i = 0; i < camera::config::MAX_REFERENCE_POINTS; i++) {
        const auto& rpos = real_pos[i].GetArray();
        realRefPoints[i].x = rpos[0].GetDouble();
        realRefPoints[i].y = rpos[1].GetDouble();
        realRefPoints[i].z = rpos[2].GetDouble();
#ifdef PRINT_DEBUG_MSG
        fmt::print("[FUNCTION][loadCam][VAR][realRefPoints][{} {} {}]\n", realRefPoints[i].x, realRefPoints[i].y,
                   realRefPoints[i].z);
#endif
    }
    const auto& virtual_pos = document["virtual_point_references"];
    for (int i = 0; i < camera::config::MAX_REFERENCE_POINTS; i++) {
        const auto& vpos = virtual_pos[i].GetArray();
        virtualRefPoints[i].x = vpos[0].GetDouble();
        virtualRefPoints[i].y = vpos[1].GetDouble();
#ifdef PRINT_DEBUG_MSG
        fmt::print("[FUNCTION][loadCam][VAR][virtualRefPoints][{} {}]\n", virtualRefPoints[i].x, virtualRefPoints[i].y);
#endif
    }

#ifdef PRINT_DEBUG_MSG
    fmt::print("[FUNCTION][loadCam][LEAVING]\n");
#endif

    return camera::camera_t(camID, camPos, virtualRefPoints, realRefPoints, finished);
}

int64_t controller::getFileSize(const char* fileName)
{
    // in our case binary flag is not necessary
    // but for safety reason we should attached it
    std::ifstream file(fileName, std::ios::binary | std::ios::ate);
    if (!file.is_open()) return -1;
    return file.tellg();
}

void controller::storeCamera([[maybe_unused]] const char* fileName)
{
}

} // namespace core
