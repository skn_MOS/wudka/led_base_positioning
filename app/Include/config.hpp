#pragma once
#include <chrono>
#include <string>

namespace core {
// default capture width and height
constexpr int FRAME_WIDTH = 640;
constexpr int FRAME_HEIGHT = 480;
constexpr const char* CONFIG_CURRENT_VERSION = "v1.0";
} // namespace core

namespace core::camera::config {
constexpr int MAX_CAMERA_COUNT = 2;
constexpr int NECESSAY_NUMBER_OF_LINES = 2;
constexpr int MAX_CAMERA_FRAMES_COUNT = 2;
constexpr int BASE_DELAY = 5;
constexpr std::chrono::milliseconds CAPTURE_IMAGE_DELAY{BASE_DELAY};
constexpr int MAX_TRACKED_POINTS = 1;
constexpr int MAX_REFERENCE_POINTS = 3;
} // namespace core::camera::config

namespace core::ui::config {
// max number of objects to be detected in frame
constexpr int MAX_NUM_OBJECTS = 50;
// minimum and maximum object area
constexpr int MIN_OBJECT_AREA = 5 * 5;
constexpr int MAX_OBJECT_AREA = FRAME_HEIGHT * FRAME_WIDTH / 1.5;
// names that will appear at the top of each window
extern const std::string mainWindow;
extern const std::string hsvWindow;
extern const std::string thresholdedWindow;
extern const std::string MorphologicalWindow;
extern const std::string trackbarWindow;

} // namespace core::ui::config