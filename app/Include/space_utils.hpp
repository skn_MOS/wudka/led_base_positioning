#pragma once
#include "config.hpp"
#include "static_vector.hpp"
#include <utility>
#include <vector>

#include <intersection.hpp>

namespace core::camera {
using pointF_3D = plane::D3::vector<double>;
struct line_t {
    pointF_3D first, second;
    friend bool operator<(const line_t& l1, const line_t& l2)
    {
        return l1.second.z < l2.second.z;
    }
};

class intersection_manager {
    // lines on which the point was detected
    static_vector<line_t, camera::config::MAX_CAMERA_COUNT> lines;
    pointF_3D position_3d;
    intersection_manager() = default;
    ~intersection_manager() = default;

public:
    void clear_lines();
    void add_line(pointF_3D& a, pointF_3D& b) noexcept;
    void calculate_position_3d();
    void debug_lines();
    void debug_point();

    size_t lines_count()
    {
        return lines.current_size();
    }

    static intersection_manager& get_instance()
    {
        static intersection_manager inst;
        return inst;
    }
};
} // namespace core::camera