#pragma once
#include "user_manager.hpp"
#include <filesystem>
#include <string>
#include <vector>

namespace core {
enum CAMERA_ID { CAM_0 = 0, CAM_1 = 2 };

using directory_info_t = std::vector<std::string>;

class controller {
    core::ui::userManager uiManager;
    const bool& finished;
    std::array<camera::camera_t, core::camera::config::MAX_CAMERA_COUNT> cameras;

public:
    controller(std::string& c);
    void run();

    camera::camera_t loadCam(const char* fileName);
    directory_info_t load_directory_files(const char* dirPath);
    void storeCamera(const char* fileName);
    int64_t getFileSize(const char* fileName);
};
} // namespace core