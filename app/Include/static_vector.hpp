#pragma once
#include "config.hpp"
#include <array>

namespace core {
template <typename T, size_t N>
class static_vector : public std::array<T, N> {
    size_t pos = 0;

public:
    static_vector() = default;
    ~static_vector() = default;
    void push_back(T&& val)
    {
        this->operator[](pos) = val;
        pos++;
    }

    void safe_push_back(T&& val)
    {
        this->operator[](pos) = val;

        if (pos < this->size()) pos++;
    }

    size_t current_size()
    {
        return pos;
    }
    decltype(auto) current_pos()
    {
        return (this->begin() + pos);
    }

    void clear()
    {
        pos = 0;
    }
};

} // namespace core
