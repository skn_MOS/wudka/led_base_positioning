#pragma once

#include "config.hpp"
#include <array>
#include <camera.hpp>
#include <opencv2/core.hpp>

namespace core::ui {
class userManager {
    // hold reference to camera frame
    std::array<core::camera::camera_t*, core::camera::config::MAX_CAMERA_COUNT> camera_ref;

    int cameraCount = 0;
    int activeCameraId = 0;
    core::image::color_range* activeTrackbars;
    bool isFinished = false;

public:
    void registerCamera(core::camera::camera_t* c);

    // this function naver returns
    // unless user requested to finish activity
    void startService();
    void createTrackers();
    const bool& getStatusRef()
    {
        return isFinished;
    }

private:
    void handleKeyboard(int key);
    bool setActiveCamera(int camId)
    {
        if (camId >= cameraCount) return false;
        activeCameraId = camId;
        return true;
    }
};

} // namespace core::ui
