#pragma once
#include <opencv2/core.hpp>

namespace core::image {
struct color_range {
    int H_MIN = 0;
    int H_MAX = 255;
    int S_MIN = 0;
    int S_MAX = 255;
    int V_MIN = 0;
    int V_MAX = 255;
};

void morphOps(cv::Mat& thresh);
bool getTrackedObjectPosition(int& x, int& y, cv::Mat& threshold, cv::Mat& cameraFeed);
void createTrackbars(color_range& cr);
void on_trackbar(int, void*) __attribute__((weak));
} // namespace core::image
