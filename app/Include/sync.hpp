#pragma once
#include "core_types.hpp"
#include <array>
#include <atomic>
#include <mutex>

namespace core {
class camera_synchronizer {
public:
    using atomic_array = std::array<std::atomic<line_state>, camera::config::MAX_CAMERA_COUNT>;
    atomic_array lineState{};
    std::mutex mut;

    void newLineAdded(int camID) noexcept
    {
        lineState[camID] = line_state::READY;
    }

    decltype(auto) getState(int camID)
    {
        return lineState[camID];
    }

    void reset(int camID)
    {
        lineState[camID] = line_state::EMPTY;
    }

    void invalidate(int camID)
    {
        lineState[camID] = line_state::INVALID;
    }

    void reset_all()
    {
        for (int id = 0; id < camera::config::MAX_CAMERA_COUNT; id++) lineState[id] = line_state::EMPTY;
    }

    void invalidate_all()
    {
        for (int id = 0; id < camera::config::MAX_CAMERA_COUNT; id++) lineState[id] = line_state::INVALID;
    }

    void block_all()
    {
        for (int id = 0; id < camera::config::MAX_CAMERA_COUNT; id++) lineState[id] = line_state::READY;
    }

    static camera_synchronizer& get_instance()
    {
        static camera_synchronizer inst;
        return inst;
    }

private:
    camera_synchronizer() = default;
    ~camera_synchronizer() = default;
};
} // namespace core