#pragma once
#include <atomic>
#include <thread>

#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/videoio/videoio_c.h>

#include "common_types.hpp"
#include "config.hpp"
#include "core_types.hpp"
#include "image_utils.hpp"

namespace core::ui {
class userManager;
}

namespace core::camera {
using pointI_2D = plane::D2::vector<int>;
using pointF_3D = plane::D3::vector<double>;
using pointI_3D = plane::D3::vector<int>;

using ref2DPointArray = pointI_2D[config::MAX_REFERENCE_POINTS];
using ref3DPointArray = pointF_3D[config::MAX_REFERENCE_POINTS];

class camera_t {
    friend class core::ui::userManager;

    inline static int next_camera_id = 0;
    int currentCammera;

    cv::VideoCapture cam;
    cv::Mat buffer[config::MAX_CAMERA_FRAMES_COUNT];
    cv::Mat* currentFrame;
    cv::Mat *frameToRender, *frameHSVToRender, *thresholdFrameToRender;

    std::thread cameraFeeder, imageProcessor;
    std::atomic<bool> getNextFrame;
    core::image::color_range cr;
    const bool* isFinished;

    // tracking stuff
    pointI_2D trackedPoint{FRAME_WIDTH / 2, FRAME_HEIGHT / 2}; // start in the middle of frame
    ref2DPointArray referencePoints;                           // reference point on virtual plane

    pointF_3D realCameraPos;            // camera postion in real word
    pointF_3D pointPosOnPlane;          // tracked point position after placing it on real plane
    ref3DPointArray realPointReference; // punkty plaszczyzny odniesienia

public:
    camera_t() = default;

    camera_t(int camId, const bool& finishedSignal);

    camera_t(int camId, pointF_3D camPos, ref2DPointArray virtualRefPos, ref3DPointArray realRefPos, const bool& finishedSignal);

    ~camera_t();
    camera_t& operator=(const camera_t& c) = delete;
    camera_t& operator=(camera_t&& c);
    void getImage();
    void drawLines(cv::Mat& frame);
    void processImage();
    cv::Mat* loadNextFrame();
    void startService();
    bool isActive()
    {
        return cam.isOpened();
    }

    void setRealCamPosition(pointF_3D pos)
    {
        this->realCameraPos = pos;
    }

    void setRealRefPosition(int idx, pointF_3D pos)
    {
        if (idx >= config::MAX_REFERENCE_POINTS) return;
        realPointReference[idx] = pos;
    }

    void setVirtualRefPosition(int idx, pointI_2D pos)
    {
        if (idx >= config::MAX_REFERENCE_POINTS) return;
        referencePoints[idx] = pos;
    }

    // tracking functions
    void captureReferencePoint(int id)
    {
        referencePoints[id].x = trackedPoint.x;
        referencePoints[id].y = trackedPoint.y;
    }

    void frame_debug();
    void plane_debug();
    pointF_3D convertToPointInSpace(int inp_x, int inp_y);
};
} // namespace core::camera
