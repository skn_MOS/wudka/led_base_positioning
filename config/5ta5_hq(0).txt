{
    "version"                 : "v1.0",
    "camera_id"               : 2,
    "camera_position"         : [-0.9, 0.0, 1.0],
    "real_point_references"   : [[1.8, -1.0, 1.0],[1.8, 0.0, 1.0],[1.8, 0.0, 2.0]],
    "virtual_point_references": [[400, 400], [200, 400],[200, 200]]
}